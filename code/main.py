import pandas as pd
import sys
sys.path.append('/root/capsule/code')
from utils import *
import re
import os
import gbwrapper as gb

def main():
    # read in input arguments
    args = sys.argv
    combo_file = args[1]
    feature_file = args[2]
    
    # parse input files
    df, backbone, region = parse_combo_file(combo_file)
    i = backbone.find(region)
    cloning_backbone = backbone[i+len(region):] + backbone[:i]
    name2seq, seq2name, name2supp = parse_feature_file(feature_file)
    # check for repeated column names and determine basis numbers
    basis = get_basis(df)
    constants = (df, seq2name, basis)
    # call recursive function
    combos = generate_combinations([], [], [], 0, [], constants)

    # TEMPORARY CODE
    # compare sequences generated by recursive funciton with sequences generated from Aarti's file
    # dan_inserts = [x[0] for x in combos]
    # dan_seqs = [backbone.replace(region, insert).upper() for insert in dan_inserts]
    # aarti_file = '/root/capsule/data/ersin_libraries/Ersin -Assay verification plasmids-updated-NLS.xlsx'
    # df = pd.read_excel(aarti_file, skiprows=3)
    # feature_order = ['U6-PBS-Terminator', 'CMV-NLS-Cas9', 'linker-RT-NLS', 'GSG', 'T2A']
    # aarti_seqs = []
    # for i in range(df.shape[0]):
    #     row = df.iloc[i,]
    #     feature_seqs_list = [row[feat].upper() for feat in feature_order]
    #     insert2 = ''.join(feature_seqs_list)
    #     bb_plasmid = row['BACKBONE']
    #     region2 = row['region to replace in backbone']
    #     plasmid = bb_plasmid.replace(region2, insert2).upper()
    #     aarti_seqs.append(plasmid)
    # print('check my sequences unique:', len(dan_seqs) == len(set(dan_seqs)))
    # print('check Aarti sequences unique:', len(aarti_seqs) == len(set(aarti_seqs)))
    # print('check same # of sequences:', len(dan_seqs) == len(aarti_seqs))
    # print('check my sequence list is subset of Aarti sequences:', sum([x in aarti_seqs for x in dan_seqs]) == len(aarti_seqs))

    # create gbrecords and write to file
    os.makedirs('/root/capsule/results/genbank_files', exist_ok=True)
    plasmids = []
    names = []
    supp_features = [name for name in name2seq if name2supp[name]]
    colors = generate_hex_colors(1000)
    for insert, name, part_lens in combos:
        plasmid = backbone.replace(region, insert).upper()
        plasmids.append(plasmid)
        names.append(name)
        gbrecord = gb.string2record(plasmid, name=name)
        start = plasmid.find(insert)
        part_names = name.split('_')
        if len(part_names) != len(part_lens):
            sys.exit('ERROR: part_lens should have same length as part_names')
        for i in range(len(part_lens)):
            part_len = part_lens[i]
            part_name = part_names[i]
            a = start
            b = (a + part_len) % len(plasmid)
            feat_seq = plasmid[a:b]
            if feat_seq in seq2name:
                feat_name = seq2name[feat_seq]
                strand = 1
            elif rc(feat_seq) in seq2name:
                feat_name = seq2name[rc(feat_seq)]
                strand = -1
            else:
                sys.exit('ERROR: feature not found in feature_file')
            gbrecord = gb.addfeature(gbrecord, a, b, strand, name=feat_name, color=colors[i])
            start = b
        
        # annotate any supplemental features present in plasmid sequence
        concat = plasmid + plasmid
        for feat_name in supp_features: 
            feat_seq = name2seq[feat_name]
            for match in re.finditer(feat_seq, concat):
                if match.start() < len(plasmid):
                    i += 1
                    start, end = match.span()
                    if end > len(plasmid):
                        end = end % len(plasmid)
                    gbrecord = gb.addfeature(gbrecord, start, end, 1, name=feat_name, color=colors[i])
            for match in re.finditer(rc(feat_seq), concat):
                if match.start() < len(plasmid):
                    i += 1
                    start, end = match.span()
                    if end > len(plasmid):
                        end = end % len(plasmid)
                    gbrecord = gb.addfeature(gbrecord, start, end, -1, name=feat_name, color=colors[i])

        # write sequence object to genbank file with path of your choosing. 
        filename = '/root/capsule/results/genbank_files/%s.gb' % name
        gb.writetofile(gbrecord, filename)

    print('Generated %d plasmid sequences.' % len(plasmids))
    if len(plasmids) != len(set(plasmids)):
        sys.exit('ERROR: There is a duplicate in the list of final plasmid sequences')
    
    
main()