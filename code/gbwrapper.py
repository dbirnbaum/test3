from Bio import SeqIO
from Bio.SeqFeature import SeqFeature, FeatureLocation, ExactPosition, OrderedDict, CompoundLocation
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import BiopythonWarning
import warnings
from datetime import date
import os


#convert sequence string the biopythin sequence object
def string2record(plasmidseqstring, name='default', description = 'auto-generated'):

    plasmidseq = plasmidseqstring.upper()   
    today=date.today()
    #build base record
    gbrecord = SeqRecord(
        Seq(plasmidseq),
        id=name,
        name=name,
        description=description,
        annotations={'molecule_type': 'ds-DNA',
                     'topology': 'circular',
                     'date': str(today.day)+'-'+str(today.month)+'-'+str(today.year)
                    }
    )

    return gbrecord
    

#add features to sequence object
#start and end are pythonic indeces. 
#direction is 1 and -1 for forward and reverse
#if start >= end, then the annotation must overlap index 0, so a compound annnotation is made
#color code is in hex - '#e63577' Tessera pink, '#2c279f' Tessera blue, '#b4abac' grey, 
# '#9eafd2' light blue, '#75c6a9' seafoam green
def addfeature(record, start, end, direction=1, name=None, color = '#e63577'):
    #record = copy.deepcopy(record)
    if name is None:
        name = ''
      
    seqlen = len(record._seq._data)

    if end > start:
        sf = SeqFeature(FeatureLocation(ExactPosition(start), ExactPosition(end),strand=direction))
    else:
        sf = SeqFeature(CompoundLocation([FeatureLocation(ExactPosition(start), ExactPosition(seqlen), strand=direction), 
                                          FeatureLocation(ExactPosition(0), ExactPosition(end), strand=direction)], 'join'),
                        location_operator='join')
        
    sf.type = "misc_feature"
    sf.qualifiers = OrderedDict([('label', [name]),
                  ('ApEinfo_revcolor', [color]),
              ('ApEinfo_fwdcolor', [color])])
    
    record.features.append(sf)
    return record
    

#write sequence object to genbank file
def writetofile(gbrecord, path):
    path = os.path.expanduser(path)
    with warnings.catch_warnings():
        warnings.simplefilter('ignore', BiopythonWarning)
        with open(path,'w') as f:
            SeqIO.write(gbrecord, f, format='gb')